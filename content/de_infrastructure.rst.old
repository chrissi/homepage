menu: Infos 
group: index
order: 10
template: base.html


Infos
=====

Tickets
-------

Die Teilnahme am Hacken Open Air wird dieses Jahr 160 € kosten.
Im Eintritt sind enthalten:

* Eine coole Location zum Hacken und Chillen
* Die Möglichkeit an Vorträgen und Workshops teilzunehmen oder selber zu
  organisieren
* Ein überdachtes Hackcenter
* Frühstück am Frühstücksbuffet
* Warmes Abendessen (auch vegan)
* W-LAN, Ethernet und DECT auf dem Campinggelände
* Camping mit deinem Zelt incl. Strom, warmer Dusche und Sanitäreinrichtungen

Zusätzlich gibt es vergünstigte Tickets für Junghacker zwischen 12 und 16 Jahren.

Die Teilnahme für Jüngsthacker ist frei.

Tagestickets gibt es sowohl im Vorverkauf als auch vor Ort.

Wenn Du in deinem Fahrzeug, Camper oder Wohnmobil auf dem Gelände schlafen möchtest
benötigst Du zusätzlich ein Camper-Ticket.
Beachte: Die Anzahl der Camper-Tickets ist begrenzt.

In unserem Presale sind außerdem T-Shirts verfügbar.
Du kannst deine T-Shirts während der Veranstaltung abholen.

An- und Abreise
---------------

Die reguläre Anreise ist am Dienstag ab mittags.
Die reguläre Abreise ist am Samstag nachmittags.

Wer uns beim Aufbauen oder Abbauen helfen möchte meldet sich
am besten kurz unter kontakt@hackenopenair.de .

Vorträge, Workshops, Self organized Sessions
--------------------------------------------

Auf dem HOA wird es die Gelegenheit geben Vorträge, Workshops oder andere
Self organized Sessions durchzuführen.
Die Organisation findet im
`Wiki <https://wiki.hackenopenair.de/public/workshops>`__ statt.

Sanitär
-------

Auf dem Welfenhof bei Gifhorn stehen uns feste Toiletten zur Verfügung.

Zusätzlich wird es eine Stelle zur Trinkwasserversorgung geben.

Corona
------

Unser Ziel ist es, für dich und alle anderen Teilnehmer auf dem HOA ein sicheres
Umfeld zu schaffen und möglichst allen Personen die Teilnahme zu ermöglichen.
In diesem Jahr ergibt sich für uns (wieder) die Herausforderung, dass wir dieses
sichere Umfeld im Kontext des neuen Corona-Virus (SARS-CoV-2) ermöglichen
wollen.

* Bitte teste dich möglichst direkt vor der Anreise mit einem Antigen-Schnelltest.
* Bitte teste dich während Veranstaltung jeden Tag.
* Bitte trage in den Innenräumen unseres festen Gebäudes möglichst eine FFP2-Maske.
* Solltest Du während der Veranstaltung ein positives Testergebnis haben
  sondere dich von den anderen Teilnehmern ab und wende dich an die Orga.
* Es gibt in diesem Jahr keine zentrale Kontaktdatenerfassung.
  Nutze bitte die Corona-Warn-App (oder ähnliches).

Diese Regeln haben wir auch noch einmal als `PDF </hygienekonzept.pdf>`__ zusammengestellt.

Strom
-----

Auf dem Platz wird es eine Strom-Grundversrogung geben.
Notebooks, Gadgets und buntes Licht können betrieben und geladen werden.
Rechne aber damit, dass der nächste Verteiler auch bis zu 50m von deinem Zelt entfernt
sein kann!
Wir rechnen pro Hacker mit einem mittleren Energiebedarf (aus Strom) von
unter 100 W.
Wenn Du also strombetriebene Pizza-Backöfen, Wasserkocher, Whirlpools oder
ähnliche Geräte mitbringen willst sprich das vorher mit uns ab.

Kommunikation
-------------

Auf dem Camp sind LAN und WLAN verfügbar.
Wir hoffen, dass wir unser Netzwerk mit ausreichender Bandbreite ans Internet
anbinden können.
Wahrscheinlich wird es auch DECT geben.

FTTT - Fiber to the tent
------------------------

Neben klassischem Kupfernetzwerk wird diesem Jahr auch Fiber to the Tent
verfügbar sein.
Bei diesem kostenlosen Mehrwertdienst bekommst du die gewohnte Leistung des
HOA-Netzwerkes mit dem guten Gefühl endlich im Cyber^WFiberspace angekommen
zu sein.

PMR
---

Zur Organisation von Auf- und Abbau werden wir PMR-Funk (PMR446) nutzen.
Wir werden eine kleine Anzahl Funkgeräte für Engel dabei haben.
Wenn du selber eins zur Verfügung hast: bring es mit.

Hunde
-----

Um für möglichst alle Teilnehmer eine angenehme Veranstaltung zu ermöglichen
sind Hunde auf dem Gelände nicht gestattet.
Blinden- und andere Diensthunde sind von dieser Regelung natürlich ausgenommen.

Anfahrt
-------

.. raw:: html

   <p>
   <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=9.580078125%2C52.07022125944487%2C11.633148193359377%2C52.8525471567007&amp;layer=mapnik&amp;marker=52.46257%2C10.604559999999992" style="border: 1px solid black"></iframe><br/><small><a target="_blank" href="https://www.openstreetmap.org/?mlat=52.46257&amp;mlon=10.60456#map=10/52.46257/10.60456">Größere Karte anzeigen</a> <a href="geo:52.46257,10.60456">Geolink</a></small><br>
   Das Hacken Open Air findet am <a href="https://osm.org/go/0G7B2YRAL?m=" >
   Pfadfinderheim Welfenhof</a> in Gifhorn
   - ca. 25km nördlich von Braunschweig - statt.
   </p>
   <p>
   Die Adresse für Navigationsgeräte ist:<br>
   <address>
   III. Koppelweg 6<br>
   38518 Gifhorn<br>
   </address>
   </p>

Die Anfahrt erfolgt über den II. Koppelweg und geht dann über den Barnbruchsweg.
Fahrt nicht über den III. Koppelweg!
Der ist mit einer Schranke verschlossen.

Hier gibt es noch noch eine
`Anfahrtsbeschreibung </Welfenhof_Anfahrt1.jpg>`__ (CW: Layout).

Shuttleservice Bahnhof
----------------------

Um euch die Planung der Anreise zu erleichtern, bieten wir einen Shuttleservice für Bahnreisende an.
Ruf uns an oder schreibe uns eine SMS um eine Abholung zu vereinbaren: 0162 39 27 117
