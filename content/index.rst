menu: Top
group: index
order: 1
template: page.html
ttmm: 09.07 bis 13.07.2024 in Gifhorn


Titel
-----

Das HOA24 findet von Dienstag bis Samstag (09. - 13.07.2024) statt.

Stay tuned!

Das Hacken Open Air ist feinstes Hacker-Camping, ausgerichtet vom `Stratum 0 <https://stratum0.org>`__.
Gemeinsam mit euch wollen wir in der Natur tüfteln, diskutieren und kreativ sein.

Die Location ist wieder das Pfadfinderheim Welfenhof in Gifhorn, ganz in der Nähe von Braunschweig. 
Adresse::

    Pfadfinderheim Welfenhof
    III. Koppelweg 6
    38518 Gifhorn

Wir freuen uns auf euch!

.. image:: faehnchen.jpeg
   :width: 600px
   :title: Viele bunte Faehnchen an einer Schnur an einem hohen Mast.

.. image:: leuchtebunt.jpeg
   :width: 600px
   :title: Viele Zelte mit bunten Lichtern bei Nacht.

.. image:: pool.jpeg
   :width: 600px
   :title: Pool mit schwimmendem Einhorn, Lastenrad und Lagerfeuer davor mit bunten Lichtern bei Nacht.

