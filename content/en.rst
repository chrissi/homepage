menu: Top
group: en
order: 1
template: page.html
ttmm: 09.07 bis 13.07.2024 in Gifhorn


Topic
-----

Save the date! HOA24 will take place from Tuesday to Saturday (09. - 13.07.2024)!

Stay tuned!

The Hacken Open Air is best hacker camping, hosted by the `Stratum 0 <https://stratum0.org>`__. Together with you, we want to tinker, discuss and be creative in the middle of the nature.

The location will once again be the Pfadfinderheim Welfenhof in Gifhorn, close to Brunswick. 
Adress::

    Pfadfinderheim Welfenhof
    III. Koppelweg 6
    38518 Gifhorn

We are looking forward to see you!

.. image:: faehnchen.jpeg
   :width: 600px
   :title: Flagpole with many colorful flags.

.. image:: leuchtebunt.jpeg
   :width: 600px
   :title: Many colorful light with tents at night.

.. image:: pool.jpeg
   :width: 600px
   :title: Swimming pool with unicorn, cargo bike and camp fire with colorful light at night (@shoragan CC-BY-SA).

