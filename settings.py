import os
import flamingo

THEME_PATHS = [
        'theme/',
]

PLUGINS = [
    'flamingo.plugins.Redirects',
    'flamingo.plugins.rstPygments',
    'plugins/title.py::Title',
    'flamingo.plugins.Thumbnails',
]

HTML_PARSER_RAW_HTML = True

@flamingo.hook("contents_parsed")
def fix_headings(context):
    for content in context.contents:
        if os.path.splitext(content["path"])[-1] == ".html":
            continue
        cb = content["content_body"]
        cb = cb.replace("h3>", "h4>")
        cb = cb.replace("h2>", "h3>")
        content["content_body"] = cb

POST_BUILD_LAYERS = [
        'downloads',
]
