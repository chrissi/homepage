# HackenOpenAir.de

Dieses Repository enthält die Quellen zur Webseite hackenopenair.de.

Hackenopenair.de wird mit dem static-site-generator [Flamingo](https://flamingo-web.org) erstellt.

## Deployment

Die Webseite wird ausgerollt, sobald du Änderungen auf die Branches `master` oder `testbed` pushed.

Der Branch `master` wird auf https://hackenopenair.de ausgerollt.
Der Branch `testbed` wird auf https://testbed.hackenopenair.de ausgerollt.

Das Ausrollen dauert in der Regel zwischen ein und zwei Minuten.
Den Prozess kann man [im Gitlab](https://gitli.stratum0.org/HOA/homepage/-/jobs) verfolgen.

Möchtest du deine Änderungen nicht veröffentlichen, so lege sie auf einen anderen branch oder
forke das Projekt.

## Inhalte auf deinem Rechner schreiben

Inhalte werden im Ordner `content/` geschrieben.
In der Ausgabe werden zwei lange Webseiten (und das Impressum) erzeugt.
Zu welcher langen Seite ein `rst`-Dokument gehört wird in den Metadaten festgelegt.
Dazu die Anfahrtsbeschreibung als Beispiel:

```
menu: Anfahrt
group: index
order: 3
template: base.html


```

Dieses Dokument wird in die Seite `index` (was bei uns die Sprache `de` ist) gerendert.
Es taucht dabei an der 3. Position auf der Seite auf und wird im Menü als `Anfahrt`
dargestellt.
(Die Angabe des Templates hat hier keine Auswirkung.)
Die Metadaten werden immer mit deiner doppelten Leerzeile abgeschlossen.
Anschließend folgt in `rst` der eigentliche Artikel.


## Webseite auf deinem Rechner ansehen

Um die Webseite auf deinem Rechner zu testen hast du zwei Möglichkeiten:

### HTML erzeugen

Dies hat die geringsten Anforderungen an deine Arbeitsumgebung.
Mache in diesem Verzeichnis ein `make html` und flamingo erzeugt die HTML-
Ausgabe der Webseite:

```
chris@dauntless $ make html
. env/bin/activate && \
mkdir -p theme/static/css/ && \
pysassc -t compressed theme/sass/*.scss theme/static/css/hoa.css
. env/bin/activate && \
flamingo build -p .
```

Anschließend liegen die Ausgaben in `output/`

### Live-Server

Für den Live-Server benötigst du mindestens `python 3.5.3`.

Starte nun den Live-Server:
```
chris@dauntless $ make server 
. env/bin/activate && \
mkdir -p theme/static/css/ && \
pysassc -t compressed theme/sass/*.scss theme/static/css/hoa.css
. env/bin/activate && \
flamingo server -p . --port=8080 --host=0.0.0.0
starting server on http://localhost:8080/live-server/
```

Rufe nun in deinem Browser `http://localhost:8000/` auf, um die
Webseite zu sehen.
Unter `http://localhost:8080/live-server` bekommst du zusätzlich
noch den Debugging-Rahmen von flaming: Hier werden z.B. Seiten
beim Speichern der Quelldatei automatisch neu geladen.
